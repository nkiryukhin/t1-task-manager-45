package ru.t1.nkiryukhin.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectStartByIndexRequest extends AbstractUserRequest {

    @NotNull
    private Integer index;

    public ProjectStartByIndexRequest(@Nullable final String token) {
        super(token);
    }

}
